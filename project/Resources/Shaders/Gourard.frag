#version 330 core
struct Material {
    sampler2D diffuse;
    sampler2D specular;
    float shininess;
	
	vec3 ambient;
	bool diffuseLoaded;
	bool specularLoaded;
}; 
in vec4 colorIn;
in vec2 TexCoords;

out vec4 color;

uniform sampler2D texture_diffuse1;
uniform Material material;

void main()
{   
	color = colorIn; 
	if(material.diffuseLoaded)
		color *= texture(material.diffuse, TexCoords);
	
}