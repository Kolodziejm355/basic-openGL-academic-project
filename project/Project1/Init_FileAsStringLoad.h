#pragma once

#include <string>
#include <fstream>
#include <streambuf>

namespace Init {
	std::string Load(std::string filePath) {
		std::ifstream t(filePath);
		std::string str((std::istreambuf_iterator<char>(t)),
			std::istreambuf_iterator<char>());
		return str;
	}
}


