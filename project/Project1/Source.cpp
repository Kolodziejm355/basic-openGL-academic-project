#include "IncludedLibraries.h"

#define M_PI        3.1415926535897
#define ModelsNumber 5

//prototypes
void Do_Movement();

//global variables

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;



GLuint generateAttachmentTexture(GLboolean depth, GLboolean stencil);
void SetUpLights(Shader s);
void SetUpSpotLights(Model Spotlights[2], Model Pole[2]);
void SetUpModels(Model models[ModelsNumber]);

// The MAIN function, from here we start our application and run our Game loop
int main()
{
	srand((unsigned)time(NULL));
	GLFWwindow* window = Init::WindowInit();
	
	Shader shader[] = { Shader("../Resources/Shaders/flat.vs", "../Resources/Shaders/flat.frag"),
					   Shader("../Resources/Shaders/Gourard.vs", "../Resources/Shaders/Gourard.frag"),
					   Shader("../Resources/Shaders/Phong.vs", "../Resources/Shaders/Phong.frag")
	};


	// Load models

	Model movingObject = Model("../Resources/Models/SoccerBall_3DS/SoccerBall.3DS", 32.0f);
	movingObject.pos = { 0.0f,0.3f,0.0f };
	movingObject.rot = { 0.0f,0.0f,0.0f };
	movingObject.size = { 0.01f, 0.01f, 0.01f };
	

	Model Pole[2];
	Model Spotlights[2];
	SetUpSpotLights(Spotlights, Pole);

	Model ourModels[ModelsNumber];
	SetUpModels(ourModels);

	Plane plane;

	float angle = 0.0f;
	
	Mirror mirror({ 0.0f,5.0f,0.0f });

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		// Check and call events
		glfwPollEvents();

		//moveObject
		float radius = 20.0f;
		angle += 0.01f;
		movingObject.rot.y = -angle/M_PI *180;

		float oldX = movingObject.pos.x;
		float oldZ = movingObject.pos.z;

		movingObject.pos.x = radius / 2 * glm::cos(angle);
		movingObject.pos.z = radius / 2 * glm::sin(angle);

		float offsetX = movingObject.pos.x - oldX;
		float offsetZ = movingObject.pos.z - oldZ;
		float dist = glm::sqrt(offsetX*offsetX + offsetZ*offsetZ);

		movingObject.rot.x += 5.5;
		if (movingObject.rot.x > 360.0f)
			movingObject.rot.x -= 360.0f;

		//operate current camera
		Init::cameraHandle(movingObject.pos, glm::vec3(offsetX, 0.0f, offsetZ));


		shader[Init::ShadingModel].Use();
		SetUpLights(shader[Init::ShadingModel]);

		


		// Transformation matrices
		glm::mat4 projection = glm::perspective(Init::currentCamera->Zoom, (float)Init::WindowWidth / (float)Init::WindowHeight, 0.1f, 500.0f);
		glm::mat4 view = Init::currentCamera->GetViewMatrix();
		glm::mat4 views[2] = { mirror.portal_view(view), view };
		for (int viewInd = 0; viewInd < 2; viewInd++) {
			

			glUniform1i(glGetUniformLocation(shader[Init::ShadingModel].Program, "Blinn"), Init::Blinn);

			glEnable(GL_DEPTH_TEST);
			if (viewInd == 1) { //drawing mirror
				glClear(GL_DEPTH_BUFFER_BIT);

				GLboolean save_color_mask[4];
				GLboolean save_depth_mask;
				glGetBooleanv(GL_COLOR_WRITEMASK, save_color_mask);
				glGetBooleanv(GL_DEPTH_WRITEMASK, &save_depth_mask);
				glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
				glDepthMask(GL_TRUE);
				mirror.DrawAlternate(shader[Init::ShadingModel]);
				glColorMask(save_color_mask[0], save_color_mask[1], save_color_mask[2], save_color_mask[3]);
				glDepthMask(save_depth_mask);

				/*shader[2].Use(); //in case plane is in different shader
				SetUpLights(shader[2]);
				glUniformMatrix4fv(glGetUniformLocation(shader[Init::ShadingModel].Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
				glUniformMatrix4fv(glGetUniformLocation(shader[Init::ShadingModel].Program, "view"), 1, GL_FALSE, glm::value_ptr(views[viewInd]));
				glm::mat4 model;

				model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
				glUniformMatrix4fv(glGetUniformLocation(shader[Init::ShadingModel].Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
				plane.Draw(shader[Init::ShadingModel]);
				shader[Init::ShadingModel].Use();*/
			}
			else { //drawing normally
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
				glClearColor(0.30f, 0.45f, 0.65f, 1.0f);
				/*shader[2].Use(); //in case plane is in different shader
				SetUpLights(shader[2]);
				glm::mat4 model;

				model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
				glUniformMatrix4fv(glGetUniformLocation(shader[Init::ShadingModel].Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
				glUniformMatrix4fv(glGetUniformLocation(shader[Init::ShadingModel].Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
				glUniformMatrix4fv(glGetUniformLocation(shader[Init::ShadingModel].Program, "view"), 1, GL_FALSE, glm::value_ptr(views[viewInd]));
				plane.Draw(shader[Init::ShadingModel]);
				shader[Init::ShadingModel].Use();*/
			}

			
			
			glUniformMatrix4fv(glGetUniformLocation(shader[Init::ShadingModel].Program, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
			glUniformMatrix4fv(glGetUniformLocation(shader[Init::ShadingModel].Program, "view"), 1, GL_FALSE, glm::value_ptr(views[viewInd]));

			//RENDERING

			glm::mat4 model;

			model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));
			glUniformMatrix4fv(glGetUniformLocation(shader[Init::ShadingModel].Program, "model"), 1, GL_FALSE, glm::value_ptr(model));
				

			plane.Draw(shader[Init::ShadingModel]);
			movingObject.Draw(shader[Init::ShadingModel]);

			
			for (int i = 0; i < 2; i++)
				Spotlights[i].Draw(shader[Init::ShadingModel]);
			for (int i = 0; i < 2; i++)
				Pole[i].Draw(shader[Init::ShadingModel]);
		
			for (int i = 0; i < ModelsNumber; i++)
				ourModels[i].Draw(shader[Init::ShadingModel]);
		}

		// Swap the buffers
		glfwSwapBuffers(window);
	}

	// Clean up
	//glDeleteFramebuffers(1, &fbo);

	glfwTerminate();
	return 0;
}

GLuint generateAttachmentTexture(GLboolean depth, GLboolean stencil)
{
	// What enum to use?
	GLenum attachment_type;
	if (!depth && !stencil)
		attachment_type = GL_RGB;
	else if (depth && !stencil)
		attachment_type = GL_DEPTH_COMPONENT;
	else if (!depth && stencil)
		attachment_type = GL_STENCIL_INDEX;

	//Generate texture ID and load texture data 
	GLuint textureID;
	glGenTextures(1, &textureID);
	glBindTexture(GL_TEXTURE_2D, textureID);
	if (!depth && !stencil)
		glTexImage2D(GL_TEXTURE_2D, 0, attachment_type, Init::renderWidth, Init::renderHeight, 0, attachment_type, GL_UNSIGNED_BYTE, NULL);
	else // Using both a stencil and depth test, needs special format arguments
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, Init::renderWidth, Init::renderHeight, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	return textureID;
}

void SetUpModels(Model ourModels[ModelsNumber]) {
	//Model("../Resources/Models/cottage/Snow Covered CottageOBJ.obj", 8);
	//Model("../Resources/Models/tree/firtree3.3ds", 8);

	int type[ModelsNumber] = { 0,0,0,1,0 };

	
	for (int i = 0; i < 5; i++) {
		switch (type[i]) {
		case 0:
			ourModels[i] = Model("../Resources/Models/cottage/Snow Covered CottageOBJ.obj", 8);
			ourModels[i].size = { 0.1f,0.1f,0.1f };
			ourModels[i].rot.y = (float)(rand() % 8) * 45.0f;
			break;

		case 1:
			ourModels[i] = Model("../Resources/Models/tree/firtree3.3ds", 12);
			ourModels[i].size = { 1.5f,1.5f,1.5f };
			ourModels[i].rot.x = -90.0f;
			ourModels[i].pos.y -= 0.2f;
			break;
		}
	}
	ourModels[0].pos = { 30.0f, 0.0f, 0.0f };
	ourModels[1].pos = { 30.0f, 0.0f, 20.0f };
	ourModels[2].pos = { 10.0f, 0.0f, -20.0f };
	ourModels[3].pos = { 0.0f, 0.0f, 17.0f };
	ourModels[4].pos = { -13.0f, 0.0f, -18.0f };
}

void SetUpSpotLights (Model Spotlights[2], Model Pole[2]) {
	for (int i = 0; i < 2; i++) {
		Pole[i] = Model("../Resources/Models/spotlight/pole.3ds", (rand() % 63 + 1)*1.0f);
		Pole[i].size = { 0.3f,0.3f,0.3f };
		Pole[i].ambColor = { 0.65f, 0.33f, 0.25f };
	}
	Pole[0].pos = { -10.4f, 0.0f,-10.4f };
	Pole[0].rot = { -90.0f, -45.0f, 0.0f };
	Pole[1].pos = { 10.4f, 0.0f, -10.4f };
	Pole[1].rot = { -90.0f, 45.0f, 0.0f };


	for (int i = 0; i < 2; i++) {
		Spotlights[i] = Model("../Resources/Models/spotlight/spotlight.3DS", (rand() % 63 + 1)*1.0f);
		Spotlights[i].size = { 0.0135f,0.0135f,0.0135f };
		Spotlights[i].ambColor = { 0.55f, 0.55f, 0.55f };
	}
	Spotlights[0].pos = { -10.0f,10.65f,-10.0f };
	Spotlights[0].rot = { -30.0f, 225.0f, 0.0f };
	Spotlights[1].pos = { 10.0f,10.65f,-10.0f };
	Spotlights[1].rot = { -30.0f, -225.0f, 0.0f };
}

void SetUpLights(Shader s) {

	// Light attributes

	//colors
	glm::vec3 ambientColor(0.3f, 0.3f, 0.3f);

	glm::vec3 pointLightPos(20.0f, 200.0f, 0.0f);
	glm::vec3 pointLightColor(1.0f, 1.0f, 1.0f);
	float pointLightStrength = 400.0f;

	glm::vec3 spotLight1Pos(-10.0f, 15.0f, -10.0f);
	glm::vec3 spotLight1Color(0.1f, 0.1f, 0.6f);
	glm::vec3 spotLight1Direction(0.0f, -1.0f, 0.0f);
	float spotLight1Strength = 35.0f;

	spotLight1Direction = glm::vec3(0, 0, 0) - spotLight1Pos;

	glm::vec3 spotLight2Pos(10.0f, 15.0f, -10.0f);
	glm::vec3 spotLight2Color(0.6f, 0.1f, 0.1f);
	glm::vec3 spotLight2Direction(0.0f, -1.0f, 0.0f);
	float spotLight2Strength = 35.0f;

	spotLight2Direction = glm::vec3(0, 0, 0) - spotLight2Pos;

	float lightConstant = 1.0f;
	float lightLinear = 0.09f;
	float lightQuadratic = 0.032f;
	float spotLightCutOff = glm::cos(glm::radians(5.5f));
	float spotLightOuterCutOff = glm::cos(glm::radians(38.0f));



	//ambient
	glUniform3f(glGetUniformLocation(s.Program, "ambient"), ambientColor.x, ambientColor.y, ambientColor.z);

	// Point light
	glUniform3f(glGetUniformLocation(s.Program, "pointLight.position"), pointLightPos.x, pointLightPos.y, pointLightPos.z);
	glUniform1f(glGetUniformLocation(s.Program, "pointLight.strength"), pointLightStrength);
	glUniform1f(glGetUniformLocation(s.Program, "pointLight.constant"), lightConstant);
	glUniform1f(glGetUniformLocation(s.Program, "pointLight.linear"), lightLinear);
	glUniform1f(glGetUniformLocation(s.Program, "pointLight.quadratic"), lightQuadratic);
	glUniform3f(glGetUniformLocation(s.Program, "pointLight.color"), pointLightColor.x, pointLightColor.y, pointLightColor.z);

	// SpotLight 1 
	glUniform3f(glGetUniformLocation(s.Program, "spotLights[0].position"), spotLight1Pos.x, spotLight1Pos.y, spotLight1Pos.z);
	glUniform3f(glGetUniformLocation(s.Program, "spotLights[0].direction"), spotLight1Direction.x, spotLight1Direction.y, spotLight1Direction.z);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[0].cutOff"), spotLightCutOff);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[0].outerCutOff"), spotLightOuterCutOff);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[0].strength"), spotLight1Strength);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[0].constant"), lightConstant);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[0].linear"), lightLinear);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[0].quadratic"), lightQuadratic);
	glUniform3f(glGetUniformLocation(s.Program, "spotLights[0].color"), spotLight1Color.x, spotLight1Color.y, spotLight1Color.z);

	glUniform1i(glGetUniformLocation(s.Program, "spotLights[0].on"), Init::SpotLight1On);

	// SpotLight 2 
	glUniform3f(glGetUniformLocation(s.Program, "spotLights[1].position"), spotLight2Pos.x, spotLight2Pos.y, spotLight2Pos.z);
	glUniform3f(glGetUniformLocation(s.Program, "spotLights[1].direction"), spotLight2Direction.x, spotLight2Direction.y, spotLight2Direction.z);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[1].cutOff"), spotLightCutOff);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[1].outerCutOff"), spotLightOuterCutOff);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[1].strength"), spotLight2Strength);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[1].constant"), lightConstant);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[1].linear"), lightLinear);
	glUniform1f(glGetUniformLocation(s.Program, "spotLights[1].quadratic"), lightQuadratic);
	glUniform3f(glGetUniformLocation(s.Program, "spotLights[1].color"), spotLight2Color.x, spotLight2Color.y, spotLight2Color.z);

	glUniform1i(glGetUniformLocation(s.Program, "spotLights[1].on"), Init::SpotLight2On);
}