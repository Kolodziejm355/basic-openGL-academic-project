#pragma once

#include <random>
#include <time.h>
#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>

#include "Init_Main.h"
#include "Camera.h"
#include "Mesh.h"
#include "Mirror.h"
#include "Shader.h"
#include "Model.h"
#include "Plane.h"