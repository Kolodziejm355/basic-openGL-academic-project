## OpenGL Project ##
Academic OpenGL 3.3 project for 'Computer Graphics' course on my university in 2016.

The project features rendering a scene with a moving object and mirror effect.  
There are 3 light sources on the scene: 2 spot lights and ambient light.  
Scene can be viewed with 3 different cameras: immobile, immobile tracking ball movement and one following ball.  
It is possible to switch between 2 lightning models (Phong, Blinn) and 3 shading models (flat, Gourard, Phong).

#### Addinational libraries used: ####
-GLFW for graphical interface and mouse/key event handling  
-SOIL (Simple OpenGL Image Loader) for loading textures  
-Assimp (Open ASset Import Library) for loading models  
-GLM (OpenGL Mathematics) for mathematical operations (mainly involving matrix calculations)

#### How to use: ####
Esc - turn off application  
C - switch cameras  
A - switch shading model  
S - turn on/off first spotlight  
D - turn on/off second spotlight  
B - switch lightning model

#### Licenses: ####
-GLEW - GNU GPL  
-GLFW - zlib/libpng  
-SOIL - Public Domain  
-Assimp - 3-clause BSD license  
-GLM - The Happy Bunny License, MIT License

All models and textures used in the project were not made by me, but free to use.